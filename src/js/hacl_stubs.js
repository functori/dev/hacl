//Provides: hacl
var hacl

//Provides: hacl_loaded
//Requires: caml_failwith
function hacl_loaded() {
  caml_failwith('hacl wasm not yet loaded')
}

//Provides: _hacl_ready
//Requires: _HACL, hacl_loaded, hacl
function _hacl_ready(cb) {
  _HACL().then(function(h) {
    hacl = h;
    hacl_loaded = function() { return };
    cb()
  })
}

//Provides: _hacl_Ed25519_secret_to_public
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8
function _hacl_Ed25519_secret_to_public(pk, sk) {
  hacl_loaded();
  var pk_ptr = walloc_u8(hacl, pk);
  var sk_ptr = walloc_u8(hacl, sk);
  hacl._Hacl_Ed25519_secret_to_public(pk_ptr, sk_ptr);
  wextract_u8(hacl, pk, pk_ptr);
  return 0
}

//Provides: _hacl_Ed25519_expand_keys
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8
function _hacl_Ed25519_expand_keys(esk, sk) {
  hacl_loaded();
  var esk_ptr = walloc_u8(hacl, esk);
  var sk_ptr = walloc_u8(hacl, sk);
  hacl._Hacl_Ed25519_expand_keys(esk_ptr, sk_ptr);
  wextract_u8(hacl, esk, esk_ptr);
  return 0
}

//Provides: _hacl_Ed25519_sign_expanded
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8, wu32
function _hacl_Ed25519_sign_expanded(sig, sk, m) {
  hacl_loaded();
  var len = wu32(m.data.length);
  var sig_ptr = walloc_u8(hacl, sig);
  var sk_ptr = walloc_u8(hacl, sk);
  var m_ptr = walloc_u8(hacl, m);
  hacl._Hacl_Ed25519_sign_expanded(sig_ptr, sk_ptr, len, m_ptr);
  wextract_u8(hacl, sig, sig_ptr);
  return 0
}

//Provides: _hacl_Ed25519_sign
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8, wu32
function _hacl_Ed25519_sign(sig, sk, m) {
  hacl_loaded();
  var len = wu32(m.data.length);
  var sig_ptr = walloc_u8(hacl, sig);
  var sk_ptr = walloc_u8(hacl, sk);
  var m_ptr = walloc_u8(hacl, m);
  hacl._Hacl_Ed25519_sign(sig_ptr, sk_ptr, len, m_ptr);
  wextract_u8(hacl, sig, sig_ptr);
  return 0
}

//Provides: _hacl_Ed25519_verify
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8, wu32
function _hacl_Ed25519_verify(pk, m, sig) {
  hacl_loaded();
  var len = wu32(m.data.length);
  var pk_ptr = walloc_u8(hacl, pk);
  var m_ptr = walloc_u8(hacl, m);
  var sig_ptr = walloc_u8(hacl, sig);
  return hacl._Hacl_Ed25519_verify(pk_ptr, len, m_ptr, sig_ptr)
}

//Provides: _hacl_Ed25519_mul_g
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8
function _hacl_Ed25519_mul_g(pk, esk) {
  hacl_loaded();
  var pk_ptr = walloc_u8(hacl, pk);
  var esk_ptr = walloc_u8(hacl, esk);
  hacl._Hacl_Ed25519_mul_g(pk_ptr, esk_ptr);
  wextract_u8(hacl, pk, pk_ptr);
  return 0;
}

//Provides: _hacl_Hash_SHA2_hash_256
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8, wu32
function _hacl_Hash_SHA2_hash_256(input, dst) {
  hacl_loaded();
  var len = wu32(input.data.length);
  var input_ptr = walloc_u8(hacl, input);
  var dst_ptr = walloc_u8(hacl, dst);
  hacl._Hacl_Hash_SHA2_hash_256(dst_ptr, input_ptr, len);
  wextract_u8(hacl, dst, dst_ptr);
  return 0
}

//Provides: _hacl_Hash_SHA2_hash_512
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8, wu32
function _hacl_Hash_SHA2_hash_512(input, dst) {
  hacl_loaded();
  var len = wu32(input.data.length);
  var input_ptr = walloc_u8(hacl, input);
  var dst_ptr = walloc_u8(hacl, dst);
  hacl._Hacl_Hash_SHA2_hash_512(dst_ptr, input_ptr, len);
  wextract_u8(hacl, dst, dst_ptr);
  return 0
}

//Provides: _hacl_HMAC_compute_sha2_256
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8, wu32
function _hacl_HMAC_compute_sha2_256(dst, key, data) {
  hacl_loaded();
  var data_len = wu32(data.data.length);
  var key_len = wu32(key.data.length);
  var data_ptr = walloc_u8(hacl, data);
  var key_ptr = walloc_u8(hacl, key);
  var dst_ptr = walloc_u8(hacl, dst);
  hacl._Hacl_HMAC_compute_sha2_256(dst_ptr, key_ptr, key_len, data_ptr, data_len);
  wextract_u8(hacl, dst, dst_ptr);
  return 0
}

//Provides: _hacl_HMAC_compute_sha2_512
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8, wu32
function _hacl_HMAC_compute_sha2_512(dst, key, data) {
  hacl_loaded();
  var data_len = wu32(data.data.length);
  var key_len = wu32(key.data.length);
  var data_ptr = walloc_u8(hacl, data);
  var key_ptr = walloc_u8(hacl, key);
  var dst_ptr = walloc_u8(hacl, dst);
  hacl._Hacl_HMAC_compute_sha2_512(dst_ptr, key_ptr, key_len, data_ptr, data_len);
  wextract_u8(hacl, dst, dst_ptr);
  return 0
}

//Provides: _hacl_Blake2b_32_blake2b
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8, wu32
function _hacl_Blake2b_32_blake2b(nn, output, data, key) {
  hacl_loaded();
  var data_len = wu32(data.data.length);
  var key_len = wu32(key.data.length);
  var nn32 = wu32(nn);
  var data_ptr = walloc_u8(hacl, data);
  var key_ptr = walloc_u8(hacl, key);
  var output_ptr = walloc_u8(hacl, output);
  hacl._Hacl_Hash_Blake2b_hash_with_key(output_ptr, nn32, data_ptr, data_len, key_ptr, key_len);
  wextract_u8(hacl, output, output_ptr);
  return 0
}

//Provides: _hacl_NaCl_crypto_secretbox_easy
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8, wu32
function _hacl_NaCl_crypto_secretbox_easy(c, m, n, k) {
  hacl_loaded();
  var m_len = wu32(m.data.length);
  var m_ptr = walloc_u8(hacl, m);
  var c_ptr = walloc_u8(hacl, c);
  var k_ptr = walloc_u8(hacl, k);
  var n_ptr = walloc_u8(hacl, n);
  var r = hacl._Hacl_NaCl_crypto_secretbox_easy(c_ptr, m_ptr, m_len, n_ptr, k_ptr);
  wextract_u8(hacl, c, c_ptr);
  return r
}

//Provides: _hacl_NaCl_crypto_secretbox_open_easy
//Requires: hacl, hacl_loaded, walloc_u8, wextract_u8, wu32
function _hacl_NaCl_crypto_secretbox_open_easy(m, c, n, k) {
  hacl_loaded();
  var c_len = wu32(c.data.length);
  var m_ptr = walloc_u8(hacl, m);
  var c_ptr = walloc_u8(hacl, c);
  var k_ptr = walloc_u8(hacl, k);
  var n_ptr = walloc_u8(hacl, n);
  var r = hacl._Hacl_NaCl_crypto_secretbox_open_easy(m_ptr, c_ptr, c_len, n_ptr, k_ptr);
  wextract_u8(hacl, m, m_ptr);
  return r
}
