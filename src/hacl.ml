external ready : (unit -> unit) -> unit = "_hacl_ready"

type pk = string
type sk = string
type expanded = {
  esk: string;
  pk: pk;
}

let sk s = s
let pk s = s

module B = Bigarray.Array1
type bs = (char, Bigarray.int8_unsigned_elt, Bigarray.c_layout) B.t

let create_bs n = B.create Bigarray.char Bigarray.c_layout n

let to_bs s =
  let b = create_bs (String.length s) in
  String.iteri (fun i c -> B.unsafe_set b i c) s;
  b

let of_bs b = String.init (B.dim b) (B.unsafe_get b)

let generate () =
  sk @@ String.init 32 (fun _ -> Char.chr @@ Random.int 256)

external secret_to_public : bs -> bs -> unit = "_hacl_Ed25519_secret_to_public" [@@noalloc]

let to_public (sk : sk) =
  let p = create_bs 32 in
  secret_to_public p (to_bs (sk :> string));
  pk (of_bs p)

external sign : bs -> bs -> bs -> unit = "_hacl_Ed25519_sign" [@@noalloc]

let sign ~(sk: sk) msg =
  let signature = create_bs 64 in
  sign signature (to_bs (sk :> string)) (to_bs msg);
  of_bs signature

external verify : bs -> bs -> bs -> bool = "_hacl_Ed25519_verify" [@@noalloc]

let verify ~(pk: pk) ~signature ~msg =
  verify (to_bs (pk :> string)) (to_bs msg) (to_bs signature)

external expand_keys : bs -> bs -> unit = "_hacl_Ed25519_expand_keys" [@@noalloc]

let expand (sk: sk) =
  let esk = create_bs 96 in
  expand_keys esk (to_bs (sk :> string));
  let esk = of_bs esk in
  { pk = pk (String.sub esk 0 32); esk = String.sub esk 32 64 }

external sign_expanded : bs -> bs -> bs -> unit = "_hacl_Ed25519_sign_expanded" [@@noalloc]

let sign_expanded ~esk msg =
  let esk = to_bs ((esk.pk :> string) ^ esk.esk) in
  let signature = create_bs 64 in
  sign_expanded signature esk (to_bs msg);
  of_bs signature

external mul_g : bs -> bs -> unit = "_hacl_Ed25519_mul_g" [@@noalloc]

let fill_expanded esk =
  let p = create_bs 32 in
  mul_g p (to_bs esk);
  { esk; pk = pk (of_bs p) }

external sha256 : bs -> bs -> unit = "_hacl_Hash_SHA2_hash_256" [@@noalloc]

let sha256 s =
  let hash = create_bs 32 in
  sha256 (to_bs s) hash;
  of_bs hash

external sha512 : bs -> bs -> unit = "_hacl_Hash_SHA2_hash_512" [@@noalloc]

let sha512 s =
  let hash = create_bs 64 in
  sha512 (to_bs s) hash;
  of_bs hash

external sha256_hmac : bs -> bs -> bs -> unit = "_hacl_HMAC_compute_sha2_256" [@@noalloc]

let sha256_hmac ~key s =
  let dst = create_bs 32 in
  sha256_hmac dst (to_bs key) (to_bs s);
  of_bs dst

external sha512_hmac : bs -> bs -> bs -> unit = "_hacl_HMAC_compute_sha2_512" [@@noalloc]

let sha512_hmac ~key s =
  let dst = create_bs 64 in
  sha512_hmac dst (to_bs key) (to_bs s);
  of_bs dst

external blake2b : int -> bs -> bs -> bs -> unit = "_hacl_Blake2b_32_blake2b" [@@noalloc]

let blake2b ?(key="") ?(length=32) s =
  let dst = create_bs length in
  blake2b length dst (to_bs s) (to_bs key);
  of_bs dst

let check_size ~key ~nonce =
  if String.length key <> 32 then failwith "secretbox key must be 32 bytes long";
  if String.length nonce <> 24 then failwith "secretbox nonce must be 24 bytes long"

external secretbox : bs -> bs -> bs -> bs -> int = "_hacl_NaCl_crypto_secretbox_easy" [@@noalloc]

let secretbox ~key ~nonce ~msg =
  check_size ~key ~nonce;
  let len = String.length msg in
  let c = create_bs (len + 16) in
  if secretbox c (to_bs msg) (to_bs nonce) (to_bs key) = 0 then Some (of_bs c)
  else None

external secretbox_open : bs -> bs -> bs -> bs -> int = "_hacl_NaCl_crypto_secretbox_open_easy" [@@noalloc]

let open_secretbox ~key ~nonce ~cypher =
  check_size ~key ~nonce;
  let len = String.length cypher in
  let m = create_bs (len - 16) in
  if secretbox_open m (to_bs cypher) (to_bs nonce) (to_bs key) = 0 then Some (of_bs m)
  else None
