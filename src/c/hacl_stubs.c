#include <caml/mlvalues.h>
#include <caml/bigarray.h>
#include <caml/callback.h>
#include "Hacl_Ed25519.h"

CAMLprim value _hacl_ready(value cb) {
  caml_callback(cb, Int_val(0));
  return Val_unit;
}

CAMLprim value _hacl_Ed25519_secret_to_public(value pk, value sk) {
  Hacl_Ed25519_secret_to_public(Caml_ba_data_val(pk), Caml_ba_data_val(sk));
  return Val_unit;
}

CAMLprim value _hacl_Ed25519_expand_keys(value esk, value sk) {
  Hacl_Ed25519_expand_keys(Caml_ba_data_val(esk), Caml_ba_data_val(sk));
  return Val_unit;
}


CAMLprim value _hacl_Ed25519_sign_expanded(value sig, value esk, value m) {
  Hacl_Ed25519_sign_expanded(Caml_ba_data_val(sig), Caml_ba_data_val(esk), Caml_ba_array_val(m)->dim[0], Caml_ba_data_val(m));
  return Val_unit;
}

CAMLprim value _hacl_Ed25519_sign(value sig, value sk, value m) {
  Hacl_Ed25519_sign(Caml_ba_data_val(sig), Caml_ba_data_val(sk), Caml_ba_array_val(m)->dim[0], Caml_ba_data_val(m));
  return Val_unit;
}

CAMLprim value _hacl_Ed25519_verify(value pk, value m, value sig) {
  return Val_bool(Hacl_Ed25519_verify(Caml_ba_data_val(pk), Caml_ba_array_val(m)->dim[0], Caml_ba_data_val(m), Caml_ba_data_val(sig)));
}

CAMLprim value _hacl_Ed25519_mul_g(value pk, value esk) {
  Hacl_Ed25519_mul_g(Caml_ba_data_val(pk), Caml_ba_data_val(esk));
  return Val_unit;
}

#include "Hacl_Hash_SHA2.h"

CAMLprim value _hacl_Hash_SHA2_hash_256(value input, value dst) {
  Hacl_Hash_SHA2_hash_256(Caml_ba_data_val(dst), Caml_ba_data_val(input), Caml_ba_array_val(input)->dim[0]);
  return Val_unit;
}

CAMLprim value _hacl_Hash_SHA2_hash_512(value input, value dst) {
  Hacl_Hash_SHA2_hash_512(Caml_ba_data_val(dst), Caml_ba_data_val(input), Caml_ba_array_val(input)->dim[0]);
  return Val_unit;
}

#include "Hacl_HMAC.h"

CAMLprim value _hacl_HMAC_compute_sha2_256(value dst, value key, value data) {
  Hacl_HMAC_compute_sha2_256(Caml_ba_data_val(dst), Caml_ba_data_val(key), Caml_ba_array_val(key)->dim[0], Caml_ba_data_val(data), Caml_ba_array_val(data)->dim[0]);
  return Val_unit;
}

CAMLprim value _hacl_HMAC_compute_sha2_512(value dst, value key, value data) {
  Hacl_HMAC_compute_sha2_512(Caml_ba_data_val(dst), Caml_ba_data_val(key), Caml_ba_array_val(key)->dim[0], Caml_ba_data_val(data), Caml_ba_array_val(data)->dim[0]);
  return Val_unit;
}

#include "Hacl_Hash_Blake2b.h"

CAMLprim value _hacl_Blake2b_32_blake2b(value nn, value output, value data, value key) {
  Hacl_Hash_Blake2b_hash_with_key(Caml_ba_data_val(output), Int_val(nn), Caml_ba_data_val(data), Caml_ba_array_val(data)->dim[0], Caml_ba_data_val(key), Caml_ba_array_val(key)->dim[0]);
  return Val_unit;
}

#include "Hacl_NaCl.h"

CAMLprim value _hacl_NaCl_crypto_secretbox_easy(value c, value m, value n, value k) {
  return Val_int(Hacl_NaCl_crypto_secretbox_easy(Caml_ba_data_val(c), Caml_ba_data_val(m), Caml_ba_array_val(m)->dim[0], Caml_ba_data_val(n), Caml_ba_data_val(k)));
}

CAMLprim value _hacl_NaCl_crypto_secretbox_open_easy(value m, value c, value n, value k) {
  return Val_int(Hacl_NaCl_crypto_secretbox_open_easy(Caml_ba_data_val(m), Caml_ba_data_val(c), Caml_ba_array_val(c)->dim[0], Caml_ba_data_val(n), Caml_ba_data_val(k)));
}
