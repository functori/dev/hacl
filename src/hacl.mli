val ready : (unit -> unit) -> unit
type pk = private string
type sk = private string
type expanded = {
  esk: string;
  pk: pk;
}
val pk : string -> pk
val sk : string -> sk
val generate : unit -> sk
val to_public : sk -> pk
val sign : sk:sk -> string -> string
val verify : pk:pk -> signature:string -> msg:string -> bool
val expand : sk -> expanded
val sign_expanded : esk:expanded -> string -> string
val fill_expanded : string -> expanded
val sha256 : string -> string
val sha512 : string -> string
val sha256_hmac : key:string -> string -> string
val sha512_hmac : key:string -> string -> string
val blake2b : ?key:string -> ?length:int -> string -> string
val secretbox : key:string -> nonce:string -> msg:string -> string option
val open_secretbox : key:string -> nonce:string -> cypher:string -> string option
