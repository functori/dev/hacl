# gcc-compatible

## ed25519
- Hacl_Ed25519.*
- Hacl_Streaming_Types.h
- Hacl_Krmllib.*
- Hacl_Hash_SHA2.*
- lib_instrinsics.h
- Hacl_IntTypes_Intrisics.h
- fstar_uint128_gcc64.h
- FStar_UInt128.h
- LowStar_Endianness.h

## hmac
- Hacl_HMAC.*
- Hacl_Hash_SHA1.*
- Hacl_Hash_SHA3.*
- Hacl_Hash_MD5.*
- Hacl_Hash_Blake2s.*
- Hacl_Hash_Blake2b.*
- lib_memzero0.h
- Lib_Memzero0.c

# nacl
- Hacl_NaCl.*
- Hacl_Salsa20.*
- Hacl_MAC_Poly1305.*
- Hacl_Curve25519_51.*

# gcc-compatible/internal

## ed25519
- Hacl_Ed25519.h
- Hacl_Krmllib.h
- Hacl_Hash_SHA2.h
- Hacl_Ed25519_PrecompTable.h
- Hacl_Curve25519_51.h
- Hacl_Bignum_Base.h
- Hacl_Bignum25519_51.h

## hmac
- Hacl_HMAC.h
- Hacl_Hash_SHA1.h
- Hacl_Hash_SHA3.h
- Hacl_Hash_MD5.h
- Hacl_Hash_Blake2s.h
- Hacl_Hash_Blake2b.h
- Hacl_Impl_Blak2_Constants.h

## nacl
- Hacl_MAC_Poly1305.h

# krml

## ed25519
- lowstar_endianness.h

# krml/internal

## ed25519
- types.h
- target.h
- compat.h
