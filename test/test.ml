open Hacl
open Alcotest

let to_hex s = Hex.show @@ Hex.of_string s

let blake2b_224_ex = "836cc68931c2e4e3e838602eca1902591d216837bafddfe6f0c8cb07"
let blake2b_256_ex = "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8"
let sha2_256_ex = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
let sha2_512_ex = "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"
let hmac_256_ex = "db1383ecb7509eac2b80a50261b356229753449bfc1a4de79622265a93349975"
let hmac_512_ex = "d7ab51f19136ae6b62e86ebdeeff5005a44001b6dbcc6ed99e780c22b38735cd98e17e60a9e252e44a10c01aef7389dac8887233d01faf39c333ccb212715010"

let sk = sk (String.init 32 (fun i -> Char.chr i))
let pk_ex = "03a107bff3ce10be1d70dd18e74bc09967e4d6309ba50d5f1ddc8664125531b8"
let esk_ex = "3894eea49c580aef816935762be049559d6d1440dede12e6a125f1841fff8e6fa9d71862a3e5746b571be3d187b0041046f52ebd850c7cbd5fde8ee38473b649"
let signature_ex = "9ca53579530654d5c3df77089ef45eda613e2fedf670e96bedac4639504e5845ef4b95d5793077233dd16817b2532e9c5525872a73a4ad74b759369a9e05c102"

let cypher_ex = "df2bb4a47aff2cb19aeb8da60bc7ae0f4a0ff2429cafb934545d7804294bb0ddf42566e353f33e5b731a519620d4c12d"

let nonce = String.make 24 '\000'
let encrypt ~key msg = secretbox ~key ~nonce ~msg
let decrypt ~key cypher = open_secretbox ~key ~nonce ~cypher

let test () =
  let blake2b_224 = to_hex @@ blake2b ~length:28 "" in
  let blake2b_256 = to_hex @@ blake2b "" in
  let sha2_256 = to_hex @@ sha256 "" in
  let sha2_512 = to_hex @@ sha512 "" in
  let hmac_256 = to_hex @@ sha256_hmac ~key:"0123456789" "" in
  let hmac_512 = to_hex @@ sha512_hmac ~key:"0123456789" "" in

  let esk = expand sk in
  let pk = to_public sk in
  let signature = sign ~sk "" in
  let false_signature = String.init 64 (fun _ -> Char.chr @@ Random.int 256) in
  let signature_expanded = sign_expanded ~esk "" in

  let key = String.init 32 Char.chr in
  let msg = String.init 32 Char.chr in
  let cypher = Option.get @@ encrypt ~key msg in
  let d_msg = Option.get @@ decrypt ~key cypher in

  let test_blake224 () = (check string) "same hex" blake2b_224_ex blake2b_224 in
  let test_blake256 () = (check string) "same hex" blake2b_256_ex blake2b_256 in
  let test_sha256 () = (check string) "same hex" sha2_256_ex sha2_256 in
  let test_sha512 () = (check string) "same hex" sha2_512_ex sha2_512 in
  let test_hmac256 () = (check string) "same hex" hmac_256_ex hmac_256 in
  let test_hmac512 () = (check string) "same hex" hmac_512_ex hmac_512 in

  let test_pk () = (check string) "same hex" pk_ex (to_hex (pk :> string)) in
  let test_esk () = (check string) "same hex" esk_ex (to_hex esk.esk) in
  let test_esk_pk () = (check string) "same hex" pk_ex (to_hex (esk.pk  :> string)) in
  let test_signature () = (check string) "same hex" signature_ex (to_hex signature) in
  let test_signature_expanded () = (check string) "same hex" signature_ex (to_hex signature_expanded) in
  let test_verify () = (check bool) "same bool" true (verify ~pk ~signature ~msg:"") in
  let test_false_verify () = (check bool) "different bool" false (verify ~pk ~signature:false_signature ~msg:"") in

  let test_encrypt () = (check string) "same hex" cypher_ex (to_hex cypher) in
  let test_secretbox () = (check string) "same hex" (to_hex msg) (to_hex d_msg) in

  run ~and_exit:false "Hacl" [
    "hash", [
      test_case "blake2b 224" `Quick test_blake224;
      test_case "blake2b 256" `Quick test_blake256;
      test_case "sha2 256" `Quick test_sha256;
      test_case "sha2 512" `Quick test_sha512;
      test_case "hmac sha2 256" `Quick test_hmac256;
      test_case "hmac sha2 512" `Quick test_hmac512;
    ];
    "ed25519", [
      test_case "public key" `Quick test_pk;
      test_case "expanded key" `Quick test_esk;
      test_case "public expanded key" `Quick test_esk_pk;
      test_case "signature" `Quick test_signature;
      test_case "expanded signature" `Quick test_signature_expanded;
      test_case "verify signature" `Quick test_verify;
      test_case "wrong signature" `Quick test_false_verify;
    ];
    "secretbox", [
      test_case "encrypt" `Quick test_encrypt;
      test_case "encrypt/decrypt" `Quick test_secretbox;
    ]
  ]

let () = ready test
