all: build

build:
	@dune build src

dev:
	@dune build --profile release @runtest @doc

deps:
	@opam install . --deps-only

dev-deps:
	@opam install . --deps-only -d -t

clean:
	@dune clean

wipe: clean
	@rm -rf src/c/hacl-star

import: wipe
	@ocaml import.mlt

install: import
	@dune build -p hacl_func @install

copy-doc:
	@cp -rf _build/default/_doc/_html public
