# Minimal Hacl Bindings

Minimal binding of Hacl for functori projects.<br/>
The documentation of the library can be found [here](https://functori.gitlab.io/dev/hacl/hacl_func/Hacl/index.html).

## Install

```bash
opam pin add hacl_func.~dev https://gitlab.com/functori/dev/hacl
```

## Functionalities

- Hash functions: `sha2_256` (and hmac), `sha2_512` (and hmac), `blake2b`
- Ed25519: generate, expand, neuterize, sign, verify
- Secretbox: create, open
